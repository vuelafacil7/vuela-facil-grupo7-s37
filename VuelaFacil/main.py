# Importar módulo de flask
from flask import Flask, render_template, redirect, url_for, request
# Importar conexión con la base de datos desde controlador
import controladordb

# Crear una variable para establecer la aplicación desde flask
app = Flask(__name__)




# Ruta principal cuando abro el proyecto de página web con flask
@app.route('/')
def index(rstDB = None):
    if rstDB != None:
        rstDB = "Valor variable rstDB"
    else:
        # conexion = conexion_db()
        # cursor = conexion.cursor()
        # sql = "SELECT * FROM productos"
        # cursor.execute(sql)
        # rstDB = []
        # # rstDB = cursor.fetchone()
        # rstDB = cursor.fetchall()
        # conexion.close()
        # rstDB = "conexion"
        rstDB = controladordb.leer_datos()
    # return "<br> <h1>Hola Tripulantes</h1> <br> <h2>Aprendiendo Flask...</h2>"
    return render_template('index.html',
                            rstDB = rstDB)


@app.route('/registrar_itinerario')
def registrar_itinerario():
    return render_template('registrar_itinerario.html')

@app.route('/guardar_datos', methods = ['GET', 'POST'])
def guardar_datos():
    if request.method == 'POST':
        CiudadOrigen = request.form['CiudadOrigen']
        AeropuertoOrigen = request.form['AeropuertoOrigen']
        CodIATA_Origen = request.form['CodIATA_Origen']
        FechaSalida = request.form['FechaSalida']
        HoraSalida = request.form['HoraSalida']
        CiudadDestino = request.form['CiudadDestino']
        AeropuertoDestino = request.form['AeropuertoDestino']
        CodIATA_Destino = request.form['CodIATA_Destino']
        FechaLlegada = request.form['FechaLlegada']
        HoraLlegada = request.form['HoraLlegada']
        Valor = request.form['Valor']
        rstDB = controladordb.insertar_datos(CiudadOrigen, AeropuertoOrigen, CodIATA_Origen, FechaSalida,
HoraSalida, CiudadDestino, AeropuertoDestino, CodIATA_Destino, FechaLlegada, HoraLlegada,
Valor)
        # return f"<h1>{rstDB}</h1>"
        return render_template('guardar_datos.html',
                                rstDB = rstDB)
    return redirect(url_for('index'))

@app.route('/editar_itinerario', methods = ['GET', 'POST'])
def editar_itinerario():
    if request.method == 'POST':
        id = request.form['id']
        rstDB = controladordb.leer_datos_id(id)
        return render_template('editar_itinerario.html',
                                rstDB = rstDB)
    return redirect(url_for('index'))

@app.route('/actualizar_datos', methods = ['GET', 'POST'])
def actualizar_datos():
    if request.method == 'POST':
        id = request.form['id']
        FechaSalida = request.form['FechaSalida']
        HoraSalida = request.form['HoraSalida']
        FechaLlegada = request.form['FechaLlegada']
        HoraLlegada = request.form['HoraLlegada']
        Valor = request.form['Valor']
        rstDB = controladordb.actualizar_datos(id, FechaSalida, HoraSalida, FechaLlegada, HoraLlegada, Valor)
        # return f"<h1>{rstDB}</h1>"
        return render_template('actualizar_datos.html',
                                rstDB = rstDB)
    return redirect(url_for('index'))


@app.route('/confirmar_eliminar_datos', methods = ['GET', 'POST'])
def confirmar_eliminar_datos():
    if request.method == 'POST':
        id = request.form['id']
        rstDB = controladordb.leer_datos_id(id)
        return render_template('confirmar_eliminar_datos.html',
                                rstDB = rstDB)
    return redirect(url_for('index'))


@app.route('/eliminar_datos', methods = ['GET', 'POST'])
def eliminar_datos():
    if request.method == 'POST':
        id = request.form['id']
        rstDB = controladordb.eliminar_datos(id)
        # return f"<h1>{rstDB}</h1>"
        return render_template('eliminar_datos.html',
                                rstDB = rstDB)
    return redirect(url_for('index'))



@app.route('/informacion')
def informacion():
       return render_template('informacion.html')

@app.route('/contenido')
def contenido():
    return render_template('contenido.html')

@app.route('/contactenos')
@app.route('/contactenos/<nombre>')
@app.route('/contactenos/<nombre>/<apellido>')
@app.route('/contactenos/<nombre>/<apellido>/<telefono>')
@app.route('/contactenos/<nombre>/<apellido>/<telefono>/<email>')
def contactenos(nombre = None, apellido = None,
                telefono = None, email = None):

    if nombre != None and apellido != None and telefono != None and email != None:
        txtNomb = f"{nombre}"
        txtApel = f"{apellido}"
        txtTel = f"{telefono}"
        txtEmail = f"{email}"

    elif nombre != None and apellido != None and telefono != None:
        txtNomb = f"{nombre}"
        txtApel = f"{apellido}"
        txtTel = f"{telefono}"
        txtEmail = "..."

    elif nombre != None and apellido != None:
        txtNomb = f"{nombre}"
        txtApel = f"{apellido}"
        txtTel = "..."
        txtEmail = "..."

    elif nombre != None:
        txtNomb = f"{nombre}"
        txtApel = "..."
        txtTel = "..."
        txtEmail = "..."
    else:
        txtNomb = "Jenny"
        txtApel = "Grupo 7"
        txtTel = "344 56 77 88"
        txtEmail = "jenny_jimenezr@hotmail.com"
    
    # return f"""
    # <br> 
    # <h1>Información</h1>
    # <br>
    # <p>
    #     Nombre: {txtNomb}<br>
    #     Apellido: {txtApel} <br>
    #     Teléfono: {txtTel} <br>
    #     E-mail: {txtEmail} <br>
    # </p>
    # """
    return render_template('contactenos.html',
                            txtNomb = txtNomb,
                            txtApel = txtApel,
                            txtTel = txtTel,
                            txtEmail = txtEmail)

# Establecer el fichero principal
if __name__ == '__main__':
    app.run(debug=True)
