# Importar conexión a la base de datos
from conexiondb import conexion_db

# CRUD => Create - Read - Update - Delete

# Create =>
def insertar_datos(CiudadOrigen, AeropuertoOrigen, CodIATA_Origen, FechaSalida,
HoraSalida, CiudadDestino, AeropuertoDestino, CodIATA_Destino, FechaLlegada, HoraLlegada,
Valor):
    conexion = conexion_db()
    with conexion.cursor() as cursor:
        # sql = "INSERT INTO productos37(id, nombre, cantidad, categoria, precio) VALUES(NULL, 'Aceite', 60, 'Víveres', 6000)"
        sql = f"INSERT INTO ItinerarioVuelo(CiudadOrigen, AeropuertoOrigen, CodIATA_Origen, FechaSalida,HoraSalida, CiudadDestino, AeropuertoDestino, CodIATA_Destino, FechaLlegada, HoraLlegada,Valor) VALUES('{CiudadOrigen}', '{AeropuertoOrigen}', '{CodIATA_Origen}', '{FechaSalida}','{HoraSalida}', '{CiudadDestino}', '{AeropuertoDestino}', '{CodIATA_Destino}', '{FechaLlegada}', '{HoraLlegada}',{Valor})"
        cursor.execute(sql)
        conexion.commit()
    conexion.close()
    return "Datos guardados..."

# Read =>
# lee todos los datos
def leer_datos():
    conexion = conexion_db()
    rstDB = []
    # cursor = conexion.cursor()
    with conexion.cursor() as cursor:
        sql = "SELECT * FROM ItinerarioVuelo"
    
        cursor.execute(sql)
        # rstDB = []
        # rstDB = cursor.fetchone()
        rstDB = cursor.fetchall()
    conexion.close()
    return rstDB

# lee únicamente los datos respecto al id
def leer_datos_id(id):
    conexion = conexion_db()
    rstDB = []
    # cursor = conexion.cursor()
    with conexion.cursor() as cursor:
        sql = f"SELECT * FROM ItinerarioVuelo WHERE IdItinerario = '{id}'"
        cursor.execute(sql)
        # rstDB = []
        rstDB = cursor.fetchone()
        # rstDB = cursor.fetchall()
    conexion.close()
    return rstDB


# Update =>
def actualizar_datos(id, FechaSalida, HoraSalida, FechaLlegada, HoraLlegada, Valor):
    conexion = conexion_db()
    with conexion.cursor() as cursor:
        sql = f"UPDATE ItinerarioVuelo SET FechaSalida = '{FechaSalida}', HoraSalida = '{HoraSalida}', FechaLlegada = '{FechaLlegada}', HoraLlegada = '{HoraLlegada}', Valor = {Valor} WHERE IdItinerario = {id}"
        cursor.execute(sql)
        conexion.commit()
    conexion.close()
    return "Datos actualizados..."


# Delete =>
def  eliminar_datos(id):
    conexion = conexion_db()
    with conexion.cursor() as cursor:
        sql = f"DELETE FROM ItinerarioVuelo WHERE IdItinerario = {id}"
        cursor.execute(sql)
        conexion.commit()
    conexion.close()
    return "Datos eliminados..."
